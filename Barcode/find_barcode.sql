﻿CREATE PROCEDURE [dbo].[find_barcode]
	@barcode VARCHAR(50)
AS
BEGIN
	SELECT 
		bc.Barcode, 
		bc.CreateDate, 
		bc.Status
	FROM [dbo].[Barcode] bc
	WHERE bc.Barcode = @barcode
END
