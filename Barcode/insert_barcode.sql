﻿CREATE PROCEDURE [dbo].[insert_barcode]
	@barcode VARCHAR(50),
	@date DATETIMEOFFSET,
	@status VARCHAR(255)
AS
BEGIN
	INSERT INTO [Barcode].[dbo].[Barcode] (Barcode, CreateDate, Status)
	VALUES(@barcode, @date, @status)
END
