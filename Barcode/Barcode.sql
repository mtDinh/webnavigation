﻿CREATE TABLE [dbo].[Barcode]
(
	[Barcode]		VARCHAR(50) NOT NULL PRIMARY KEY,
	[CreateDate]	DatetimeOffSet NOT NULL,
	[Status]		VARCHAR(255) NULL
)
