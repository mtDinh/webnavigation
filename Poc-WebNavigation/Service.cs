﻿using OpenQA.Selenium;

using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Linq;

namespace Poc_WebNavigation
{
    public static class Service
    {
        //private static readonly string _url = "https://www.deutschepost.de/sendung/simpleQuery.html?locale=en_GB";
        //private static ChromeDriverService _driverService => ChromeDriverService.CreateDefaultService(@"C:\Bit bucket\Poc-WebNavigation\Driver");
        //private static ChromeOptions _options => new ChromeOptions();

        public static string GetBarcodeStatus(Barcode barcode)
        {
            var url = "https://www.deutschepost.de/sendung/simpleQuery.html?locale=en_GB";
            ChromeDriverService driverService = ChromeDriverService.CreateDefaultService(@"C:\Bit bucket\Poc-WebNavigation\Driver");
            ChromeOptions options = new ChromeOptions();

            driverService.HideCommandPromptWindow = true;
            options.AddArgument("headless");
            options.AddArguments("--proxy-server='direct://'");
            options.AddArguments("--proxy-bypass-list=*");
            options.Proxy = null;

            using (IWebDriver driver = new ChromeDriver(driverService, options))
            {
                driver.Url = url;

                IWebElement barcodeTB = driver.FindElement(By.Id("form_sendungsnummer"));
                IWebElement monthDB = driver.FindElement(By.Id("form_einlieferungsdatum_monat"));
                IWebElement dayDB = driver.FindElement(By.Id("form_einlieferungsdatum_tag"));
                IWebElement yearDB = driver.FindElement(By.Id("form_einlieferungsdatum_jahr"));
                IWebElement searchBT = driver.FindElement(By.XPath("//*[@id='sy1']/div[2]/section[1]/a"));

                var selectedDay = new SelectElement(dayDB);
                var selectedMonth = new SelectElement(monthDB);
                var selectedYear = new SelectElement(yearDB);

                barcodeTB.SendKeys(barcode.Code);
                selectedDay.SelectByValue(barcode.CreatedDate.Day.ToString());
                selectedMonth.SelectByValue(barcode.CreatedDate.Month.ToString());
                selectedYear.SelectByValue(barcode.CreatedDate.Year.ToString());

                searchBT.Click();

                IWebElement result = driver.FindElement(By.XPath("//*[@id='content']/section[1]/div[2]/section/div/table/tbody/tr[2]/td[2]"));

                return result
                    .Text;
                    //.Split(' ')
                    //.Last()
                    //.Replace(".", "-")
                    //.Remove(result.Text.Length - 1);
            }
        }
    }
}
