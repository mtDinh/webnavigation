﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using System.Linq;

namespace Poc_WebNavigation
{
    public static class Repository
    {
        private readonly static string _connection = "Data Source=DESKTOP-CILD6IE\\SQLSERVER;Initial Catalog=Barcode;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static async Task Insert(Barcode barcode)
        {
            const string sql = "[dbo].[insert_barcode]";

            var param = new
            {
                barcode = barcode.Code,
                date = barcode.CreatedDate,
                status = barcode.Status
            };

            using (var connection = await GetConnection())
            {
                var result = await connection.QueryAsync(sql, param, commandType: CommandType.StoredProcedure);
            }
        }

        public static async Task<Barcode> Get(string code)
        {
            const string sql = "[dbo].[find_barcode]";

            var param = new
            {
                barcode = code
            };

            using (var connection = await GetConnection())
            {
                var result = await connection.QueryAsync(sql, param, commandType: CommandType.StoredProcedure);

                return result.Select(x => new Barcode
                {
                    Code = x.Barcode,
                    CreatedDate = x.CreateDate
                }).SingleOrDefault();
            }
        }

        private static async Task<IDbConnection> GetConnection()
        {
            var connection = new SqlConnection(_connection);
            await connection.OpenAsync();
            return connection;
        }
    }
}
