﻿using System;

namespace Poc_WebNavigation
{
    public class Barcode
    {
        public string Code { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public string Status { get; set; }
    }
}
