﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace Poc_WebNavigation
{
    public static class ExcelService
    {
        public static IEnumerable<Barcode> ReadData()
        {
            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(@"C:\Bit bucket\Poc-WebNavigation\Excel\DEitems2.xlsx", false))
            {
                WorkbookPart workbookPart = doc.WorkbookPart;
                Sheets thesheetcollection = workbookPart.Workbook.GetFirstChild<Sheets>();
                var barcodeList = new List<Barcode>();
                
                foreach (Sheet thesheet in thesheetcollection)
                {
                    Worksheet theWorksheet = ((WorksheetPart)workbookPart.GetPartById(thesheet.Id)).Worksheet;

                    SheetData thesheetdata = (SheetData)theWorksheet.GetFirstChild<SheetData>();
                    foreach (Row thecurrentrow in thesheetdata)
                    {
                        var barcode = new Barcode();
                        foreach (Cell thecurrentcell in thecurrentrow)
                        {                            
                            //statement to take the integer value  
                            string currentcellvalue = string.Empty;
                            if (thecurrentcell.DataType != null)
                            {
                                if (thecurrentcell.DataType == CellValues.SharedString)
                                {
                                    int id;
                                    if (Int32.TryParse(thecurrentcell.InnerText, out id))
                                    {
                                        SharedStringItem item = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
                                        if (item.Text != null)
                                        {
                                            //code to take the string value
                                            barcode.Code = item.Text.Text;
                                            //barcodeList.Add(item.Text.Text + " ");
                                        }
                                        else if (item.InnerText != null)
                                        {
                                            currentcellvalue = item.InnerText;
                                        }
                                        else if (item.InnerXml != null)
                                        {
                                            currentcellvalue = item.InnerXml;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                barcode.CreatedDate = DateTime.FromOADate(Convert.ToDouble(thecurrentcell.InnerText));
                            }
                        }
                        barcodeList.Add(barcode);
                    }                    
                }

                return barcodeList;
            }            
        }
    }
}
