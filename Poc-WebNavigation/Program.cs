﻿using System;
using System.Threading.Tasks;

namespace Poc_WebNavigation
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var dataList = ExcelService.ReadData();

            foreach(var barcode in dataList)
            {
                var entity = await Repository.Get(barcode.Code);
                if (entity == null)
                {
                    barcode.Status = Service.GetBarcodeStatus(barcode);
                    await Repository.Insert(barcode);
                }                    
            }
        }
    }
}
